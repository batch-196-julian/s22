let koponanNiEugene = ["Eugene"];

koponanNiEugene.push("Vincent");
console.log(koponanNiEugene);

console.log(koponanNiEugene.push("Dennis"));
console.log(koponanNiEugene);

let removedItem = koponanNiEugene.pop();
console.log(koponanNiEugene);
console.log(removedItem);

let fruits = ["Mango", "Kiwi","Apple"];
fruits.unshift("Pineapple");
console.log(fruits);

let commputerBrands = ["Apple", "Acer","Asus","Dell"];
commputerBrands.shift();
console.log(commputerBrands);

commputerBrands.splice(1);
console.log(commputerBrands);
fruits.splice(1,2);
console.log(fruits);

koponanNiEugene.splice(0,0,"Dennis",'Alfred');
console.log(koponanNiEugene);

console.log(fruits);
fruits.splice(0,2,"Lime", "Cherry");
console.log(fruits);

let item = fruits.splice(0);
console.log(fruits);
console.log(item);

let spiritDetective = koponanNiEugene.splice(0,1);
console.log(spiritDetective);
console.log(koponanNiEugene);

let members = ["Ben", "Alan", "Alvin", "Jino",'Tine'];
members.sort();
console.log(members);


let numbers = [50,100,12,10,1];
numbers.sort();
console.log(numbers);

members.reverse();
console.log(members);

// Non -Mutator method - are not able to modify or change the oroginal array.

let carBrands = ["Vios", "Fortuner", "Crosswind", "City", "Vios","Starex"];


// indexOf

let firstIndexOfvios = carBrands.indexOf("Vios");
console.log(firstIndexOfvios);

let indexofStrarex = carBrands.indexOf("Starex");
console.log(indexofStrarex);

let indexofBeetle = carBrands.indexOf("Beetle");
console.log(indexofBeetle);

// lastindexOf

let lastIndexOfvios = carBrands.lastIndexOf("Vios");
console.log(lastIndexOfvios);

let lastIndexOfMio = carBrands.lastIndexOf("Mio");
console.log(lastIndexOfMio);

let shoeBrand = ["Jordan", "Nike", "Adidas","Converse","Sketchers"];
console.log(shoeBrand);
let myOwnedShoes = shoeBrand.slice(2);
console.log(myOwnedShoes);
console.log(shoeBrand);

let herOwnedShoes = shoeBrand.slice(0,3);
console.log(herOwnedShoes);

let heroes = ["Captain America", "Superman", "Spiderman", 'Wonder Woman', 'Hulk','Hawkeye','Dr. Strange'];

let myFavoriteHeroes = heroes.slice(2,6);
console.log(myFavoriteHeroes);
console.log(heroes);

// toString() - return our array as a string seperated by commas

let superHeroes = heroes.toString();
console.log(superHeroes);
console.log("My favorite superheroes are "+superHeroes);

let superHeroes2 = heroes.join("-");
console.log(superHeroes2);

let superHeroes3 = heroes.join(" ");
console.log(superHeroes3);

let superHeroes4 = heroes.join();
console.log(superHeroes4);

// Iterator Methods - iterates or loops over the items array

// .forEach
let counter = 0;
heroes.forEach(function(hero){
		counter++;
		console.log(counter);
		console.log(hero);
	})

let chessBoard = [

  ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

chessBoard.forEach(function(row){
	// console.log(row);
	row.forEach(function(square){
		console.log(square);
	})

})

let numArr = [5,12,30,46,40];
numArr.forEach(function(number){

	if (number%5 === 0){
		console.log(number + "is divisible by 5");
	} else {
		console.log(number + " is not divisible by 5");
	}
})

// map() - simmilar to forEach it will iterate over all items in an array and run a function for each other.

// let members = ["Ben", "Alan", "Alvin", "Jino",'Tine'];

let instructors = members.map(function(member){

	return member + " is an instructor";
})
console.log(instructors);
console.log(members);

let numArr2 = [1,2,3,4,5];

let squaresMap = numArr2.map(function(number){

	return number * number
})

console.log(squaresMap);
console.log(numArr2);


let squaresForEach = numArr2.forEach(function(number){

return number * number

})

console.log(squaresForEach);

// includes() - returns boolean which determines if the item is in the array or not.

let isAMember = members.includes("Tine");
console.log(isAMember);

let isAMember2 = members.includes("Tee Jae");
console.log(isAMember2);